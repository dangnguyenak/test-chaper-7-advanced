<?php
namespace Magenest\Movie\Observer;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\ObserverInterface;


class SetCustomText implements ObserverInterface
{
    protected $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $configPath = 'movie/moviepage/movie_textfield';
        // TODO: Implement execute() method.
        $website = $observer->getEvent()->getData('website');
        $store = $observer->getStore();
        $data = $this->scopeConfig->getValue($configPath, $storeScope);

    }
}