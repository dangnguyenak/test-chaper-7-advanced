<?php
/**
 * @copyright Copyright (c) 2016 www.magebuzz.com
 */

namespace Magenest\Movie\Controller\Adminhtml\Movielist;

use Magenest\Movie\Model\Movies;
use Magento\Backend\App\Action;

class Edit extends Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
//        $this->_model = $movieModel;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
//    protected function _isAllowed()
//    {
//        return $this->_authorization->isAllowed('');
//    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Movie::movielist');
        $resultPage->addBreadcrumb(__('Magenest Movies'), __('Magenest Movies'));
        $resultPage->addBreadcrumb(__('Movies list'), __('Movies list'));
        return $resultPage;
    }

    /**
     * Edit Staff
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        /** @var \Magenest\Movie\Model\Movies $model */
        $model = $this->_objectManager->create('Magenest\Movie\Model\Movies');
//        $model = $this->model;
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This movie no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        $this->_coreRegistry->register('magenest_movie', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Movie') : __('New Movie'),
            $id ? __('Edit Movie') : __('New Movie')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Movies list'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getTitle() : __('New Movie'));

        return $resultPage;
    }
}